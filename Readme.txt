Remote Content Module
=====================

The remote content module provides a field which allows you to enter a URL and a test format. Every time the node is
viewed, the content is fetched from the URL and rendered using the specified URL.

One usage of this is to store your pages as markdown on GitHub. Just add the markdown filter module and you
have a nice solution to fetch your node content from GitHub.

Security
--------

Think twice about what users do get the right do use this field and what formatters they can use. Giving this field to
anonymous users with the Full HTML formatter is for sure asking for trouble.
